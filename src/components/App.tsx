import React, { useEffect } from 'react';
import styles from './App.module.css';
import TodoForm from './TodoForm';
import TodoItem from './TodoItem';
import {
  addTodoThunk,
  getTodosThunk,
  removeTodoThunk,
  toggleTodoThunk,
} from '../redux/actionCreators';
import { useTypedSelector } from '../hooks/useTypedSelector';
import { useAppDispatch } from '../hooks/useAppDispatch';
import { ITodo } from '../types/types';

const App: React.FC = () => {
  const todos = useTypedSelector((state) => state.todo.todos);

  const dispatch = useAppDispatch();

  const addTask = (todoInput: string) => {
    if (!todoInput) return;
    const newTodo = {
      id: Date.now(),
      title: todoInput,
      completed: false,
    };
    dispatch(addTodoThunk(newTodo));
  };

  const handleToggle = (todo: ITodo) => {
    dispatch(toggleTodoThunk(todo));
  };

  const removeTask = (id: number) => {
    dispatch(removeTodoThunk(id));
  };

  useEffect(() => {
    dispatch(getTodosThunk());
  }, []);

  return (
    <div className={styles.wrapper}>
      <TodoForm addTask={addTask} />
      <ul className={styles.list}>
        {todos.map((item: ITodo) => (
          <TodoItem
            key={item.id}
            todo={item}
            handleToggle={handleToggle}
            removeTask={removeTask}
          />
        ))}
      </ul>
    </div>
  );
};

export default App;
