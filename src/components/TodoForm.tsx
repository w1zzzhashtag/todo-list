import React, { useState } from "react";
import { ITodoFormProps } from "../types/types";

const TodoForm: React.FC<ITodoFormProps> = ({ addTask }) => {
  const [todoInput, setTodoInput] = useState("");

  const handleSubmit = (evt: React.FormEvent<HTMLFormElement>) => {
    evt.preventDefault();
    addTask(todoInput);
    setTodoInput("");
  };

  const handleChange = (evt: React.ChangeEvent<HTMLInputElement>) => {
    setTodoInput(evt.target.value);
  };

  return (
    <form onSubmit={handleSubmit}>
      <input
        type="text"
        value={todoInput}
        placeholder="Введите задачу..."
        onChange={handleChange}
      />
      <button>Сохранить</button>
    </form>
  );
};

export default TodoForm;
