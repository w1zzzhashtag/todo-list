import React from 'react';
import { ITodoItemProps } from '../types/types';
import styles from './App.module.css';

const TodoItem: React.FC<ITodoItemProps> = ({
  todo,
  handleToggle,
  removeTask,
}) => {
  const handleClick = () => {
    handleToggle(todo);
  };

  const handleClickRemove = () => {
    removeTask(todo.id);
  };

  return (
    <li className={styles.item}>
      <p
        className={`${styles.text} ${todo.completed ? styles.done : ''}`}
        onClick={handleClick}
      >
        {todo.title}
      </p>
      <span onClick={handleClickRemove}> &#10006;</span>
    </li>
  );
};

export default TodoItem;
