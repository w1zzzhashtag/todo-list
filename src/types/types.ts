export interface ITodo {
  userId?: number;
  id: number;
  title: string;
  completed: boolean;
}

export interface ITodoFormProps {
  addTask: (todoInput: string) => void;
}

export interface ITodoItemProps {
  todo: ITodo;
  handleToggle: (todo: ITodo) => void;
  removeTask: (id: number) => void;
}

export enum todosActionTypes {
  ADD_TODO = 'ADD_TODO',
  REMOVE_TODO = 'REMOVE_TODO',
  TOGGLE_TODO = 'TOGGLE_TODO',
  GET_TODOS = 'GET_TODOS',
}

export interface ITodosState {
  todos: ITodo[];
}

export interface ITodosActionArr {
  type: todosActionTypes.GET_TODOS;
  payload: ITodo[];
}

export interface ITodosActionQbj {
  type: todosActionTypes.ADD_TODO;
  payload: ITodo;
}

export interface ITodosActionId {
  type: todosActionTypes.TOGGLE_TODO | todosActionTypes.REMOVE_TODO;
  payload: number;
}

export type TodosAction = ITodosActionQbj | ITodosActionId | ITodosActionArr;

