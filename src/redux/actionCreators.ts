import { ThunkAction } from 'redux-thunk';
import { RootState } from '../redux/index';
import { todosActionTypes, ITodo } from '../types/types';
import { AnyAction } from 'redux';
import {
  getOptionsforPostRequest,
  request,
  getOptionsforDeleteRequest,
  getOptionsforPatchRequest,
} from '../utils/utils';

export function getTodos(todosArray: ITodo[]) {
  return {
    type: todosActionTypes.GET_TODOS,
    payload: todosArray,
  };
}

export function addTodo(todo: ITodo) {
  return {
    type: todosActionTypes.ADD_TODO,
    payload: todo,
  };
}

export function removeTodo(id: number) {
  return {
    type: todosActionTypes.REMOVE_TODO,
    payload: id,
  };
}

export function toggleTodo(id: number) {
  return {
    type: todosActionTypes.TOGGLE_TODO,
    payload: id,
  };
}

type ThunkActionType = ThunkAction<void, RootState, unknown, AnyAction>;

export const getTodosThunk = (): ThunkActionType => {
  return (dispatch) => {
    request('todos?_limit=10')
      .then((res) => {
        // получаем ответ и используем
        dispatch(getTodos(res));
      })
      .catch((err) => {
        console.log(err);
      });
  };
};

export const addTodoThunk = (newTodo: ITodo): ThunkActionType => {
  return (dispatch) => {
    request('posts', getOptionsforPostRequest(newTodo))
      .then((res) => {
        // отправляем запрос, но ответ не используем
        // {
        // "id": 101, не могу передать id
        // "title": "пппп",
        // "completed": false
        // }
        dispatch(addTodo(newTodo));
      })
      .catch((err) => {
        console.log(err);
      });
  };
};

export const removeTodoThunk = (id: number): ThunkActionType => {
  return (dispatch) => {
    request('posts/1', getOptionsforDeleteRequest())
      .then((res) => {
        // res = {}, не используем
        dispatch(removeTodo(id));
      })
      .catch((err) => {
        console.log(err);
      });
  };
};

export const toggleTodoThunk = (todo: ITodo): ThunkActionType => {
  return (dispatch) => {
    request('posts/1', getOptionsforPatchRequest(todo))
      .then((res) => {
        dispatch(toggleTodo(res.id));
      })
      .catch((err) => {
        console.log(err);
      });
  };
};
