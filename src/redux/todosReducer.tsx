import {
  ITodosState,
  TodosAction,
  todosActionTypes,
} from '../../src/types/types';

const initialState: ITodosState = {
  todos: [],
};

export const todosReducer = (state = initialState, action: TodosAction) => {
  switch (action.type) {
    case todosActionTypes.GET_TODOS:
      return { ...state, todos: action.payload };
    case todosActionTypes.ADD_TODO:
      return { ...state, todos: [...state.todos, action.payload] };
    case todosActionTypes.REMOVE_TODO:
      return {
        ...state,
        todos: state.todos.filter((item) => item.id !== action.payload),
      };
    case todosActionTypes.TOGGLE_TODO:
      return {
        ...state,
        todos: state.todos.map((todo) => {
          if (todo.id === action.payload) {
            return { ...todo, completed: !todo.completed };
          }
          return todo;
        }),
      };
    default:
      return state;
  }
};
