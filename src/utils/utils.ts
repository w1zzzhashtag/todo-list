import { BASE_URL } from './constants';
import { ITodo } from './../types/types';

const checkResponse = (res: Response) => {
  if (res.ok) {
    return res.json();
  }
  return Promise.reject(`Ошибка ${res.status}`);
};

export const request = (endpoint: RequestInfo | URL, options?: RequestInit) => {
  return fetch(`${BASE_URL}${endpoint}`, options).then(checkResponse);
};

export const getOptionsforPostRequest = (todo: ITodo) => {
  return {
    method: 'POST',
    headers: {
      'Content-type': 'application/json; charset=UTF-8',
    },
    body: JSON.stringify(todo),
  };
};

export const getOptionsforDeleteRequest = () => {
  return {
    method: 'DElETE',
  };
};

export const getOptionsforPatchRequest = (todo: ITodo) => {
  return {
    method: 'PATCH',
    body: JSON.stringify(todo),
    headers: {
      'Content-type': 'application/json; charset=UTF-8',
    },
  };
};
